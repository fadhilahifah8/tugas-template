<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


// Route::get('/master', function(){
//     return view('adminLTE.master');
// });

// Route::get('/items', function() {
//     return view('items.indeks');
// });

// Route::get('/items/create', function() {
//     return view('items.create');
// });

Route::get('/master', function(){
    return view('tugasTemplate.master');
});

Route::get('/tugas1', function() {
    return view('tugasTemplateLTE.table');
});

Route::get('/tugas2', function() {
    return view('tugasTemplateLTE.data-tables');
});

Route::get('/items', function() {
    return view('tugasTemplateLTE.indeks');
});
